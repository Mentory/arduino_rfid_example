#include <Servo.h> //используем библиотеку для работы с сервоприводом

Servo servo; //объявляем переменную servo типа Servo

#include <SPI.h>
#include <MFRC522.h>

#define RST_PIN         9          // Configurable, see typical pin layout above
#define SS_PIN          10         // Configurable, see typical pin layout above

MFRC522 rfid(SS_PIN, RST_PIN);  // Create MFRC522 instance
 int serNum0;
 int serNum1;
 int serNum2;
 int serNum3;
 int serNum4;

 int goodCard [3][5] = {
    {1, 106, 14, 230, 0},
    {139, 120, 50, 0, 0},
    {8, 3, 106, 185, 0}
  };

#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27,16,2);  // Устанавливаем дисплей

void setup() {
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  servo.attach(7); //привязываем привод к порту 7
  Serial.begin(9600);   // Initialize serial communications with the PC
  while (!Serial);    // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
  SPI.begin();      // Init SPI bus
  rfid.PCD_Init();   // Init MFRC522
  delay(4);       // Optional delay. Some board do need more time after init to be ready, see Readme
  rfid.PCD_DumpVersionToSerial();  // Show details of PCD - MFRC522 Card Reader details
  Serial.println(F("Scan PICC to see UID, SAK, type, and data blocks..."));

  lcd.init();                     
  lcd.backlight();// Включаем подсветку дисплея
  lcd.print("HELLO");
  delay(2000);
  lcd.clear();

}

void loop() {
  /*
  servo.write(0); //ставим вал под 0
  delay(2000); //ждем 2 секунды
  servo.write(180); //ставим вал под 180
  delay(2000); //ждем 2 секунды
  */
  if (rfid.PICC_IsNewCardPresent()) 
  {
    if (rfid.PICC_ReadCardSerial()) 
    {
        Serial.println(" ");
        Serial.println("Карта обнаружена");
        serNum0 = rfid.uid.uidByte[0];
        serNum1 = rfid.uid.uidByte[1];
        serNum2 = rfid.uid.uidByte[2];
        serNum3 = rfid.uid.uidByte[3];
        serNum4 = rfid.uid.uidByte[4];

        //Serial.println(" ");
        Serial.println("Cardnumber:");
        Serial.print("Dec: ");
        Serial.print(rfid.uid.uidByte[0],DEC);
        Serial.print(", ");
        Serial.print(rfid.uid.uidByte[1],DEC);
        Serial.print(", ");
        Serial.print(rfid.uid.uidByte[2],DEC);
        Serial.print(", ");
        Serial.print(rfid.uid.uidByte[3],DEC);
        Serial.print(", ");
        Serial.print(rfid.uid.uidByte[4],DEC);
        Serial.println(" ");

        Serial.print("Hex: ");
        Serial.print(rfid.uid.uidByte[0],HEX);
        Serial.print(", ");
        Serial.print(rfid.uid.uidByte[1],HEX);
        Serial.print(", ");
        Serial.print(rfid.uid.uidByte[2],HEX);
        Serial.print(", ");
        Serial.print(rfid.uid.uidByte[3],HEX);
        Serial.print(", ");
        Serial.print(rfid.uid.uidByte[4],HEX);
        Serial.println(" ");

        //ПРоверка карты
        if (isGoodCard())
        {
          lcd.print("Access is allowed");
          digitalWrite(2, HIGH);
          servo.write(0); //ставим вал под 0
          delay(2000); //ждем 2 секунды
          servo.write(180); //ставим вал под 180
          delay(2000); //ждем 2 секунды
          lcd.clear();
          digitalWrite(2, LOW);
        }
        else
        {
            lcd.print("Access is denied");
            digitalWrite(3, HIGH);
            delay(2000); //ждем 2 секунды
            lcd.clear();
            digitalWrite(3, LOW);
        }
    }
  }
  rfid.PICC_HaltA();

}

bool isGoodCard()
{
  bool res = false;

  for (int i = 0; i < 3; ++i)
  {
    int a = goodCard[i][0], b = goodCard[i][1], c = goodCard[i][2], d = goodCard[i][3], e = goodCard[i][4];
    if (a == serNum0 && b == serNum1 && c == serNum2 && d == serNum3 && e == serNum4)  
    {
      res = true;
      return res;  
    }
  }  
  return res;
}

